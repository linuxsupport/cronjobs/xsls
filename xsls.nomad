job "${PREFIX}_xsls" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_xsls" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/xsls/xsls:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_xsls"
        }
      }
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      TAG = "${PREFIX}_xsls"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 256 # MB
    }

  }
}

