#!/bin/bash

AVAIL=20

for HOST in `dig +short linuxsoft.cern.ch`; do
  HTTP_CODE=`curl --max-time 60 --write-out '%{http_code}' --silent --output /dev/null http://$HOST`
  [ "$HTTP_CODE" == "200" ] && AVAIL=$(( $AVAIL + 10 ))
done

TSTAMP=`/bin/date +%s`
STATUS='unavailable'

[ $AVAIL -ge 30 ] && STATUS='degraded'
[ $AVAIL -ge 50 ] && STATUS='available'

if [[ "$NOMAD_TASK_NAME" == *"prod"* ]]; then
  COMMAND=/usr/bin/curl
else
  echo -n "[DEV] Would have run the following: curl "
  COMMAND=/usr/bin/echo
fi

DATA="{\"producer\":\"service\",\"type\":\"availability\",\"serviceid\":\"linuxsoft\",\"service_status\":\"$STATUS\",\"availabilitydesc\":\"$STATUS\",\"availabilityinfo\":\"$STATUS\",\"contact\":\"lxsoft-admins@cern.ch\",\"webpage\":\"http://linuxsoft.cern.ch\",\"timestamp\":$TSTAMP}"
echo "Sending: $DATA"
$COMMAND --max-time 60 -X POST monit-metrics.cern.ch:10012/ -d "${DATA}" -H 'Content-Type: application/json'
